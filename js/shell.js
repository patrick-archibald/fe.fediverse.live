var intranetApp = angular.module('intranetModule', []);

intranetApp.factory('communicationService', function($rootScope) {
  communicationService = {};
  console.log('communicationService created.');
  communicationService.broadcastLoaded = function(obj) {
    communicationService.sharedObject = obj;
    console.log("broadcastLoaded method with message: " + obj);
    $rootScope.$broadcast('objectShared');
  }
  return communicationService;
});

intranetApp.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($q, $rootScope) {
    return {
      'request': function(config) {
        $rootScope.$broadcast('loading-started');
        return config || $q.when(config);
      },
      'response': function(response) {
        $rootScope.$broadcast('loading-complete');
        return response || $q.when(response);
      }
    };
  });
});

app.directive("loadingIndicator", function() {
  return {
    restrict: "A",
    template: "<div>Loading...</div>",
    link: function(scope, element, attrs) {
      scope.$on("loading-started", function(e) {
        element.css({
          "display": ""
        });
      });

      scope.$on("loading-complete", function(e) {
        element.css({
          "display": "none"
        });
      });

    }
  };
});

app.controller('controller', ['$scope', '$http',
  function($scope, $http) {
    $scope.settings = {};
    $scope.load = function() {
      var url = `/Servlet?action=changeMe`;
      $scope.settings.message = null;
      $http.get(url).then(
        function(response) {
          $scope.items = response.data;
        },
        function(response) {
          $scope.settings.message = getHttpErrorText(response.data);
        });
    }

  }
]);
