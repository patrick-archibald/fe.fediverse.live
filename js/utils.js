function isBlank(str) {
  return (!str || /^\s*$/.test(str));
}

function isNotBlank(str) {
  return !isBlank(str);
}

function storageSet(name, value) {
  console.log("Saving " + name + " value is: " + value);
  localStorage.setItem(name, value);
  return false;
}

function storageGet(name, defaultValue) {
  if (localStorage == null) {
    return defaultValue;
  }
  var value = localStorage.getItem(name);
  if (isBlank(value)) {
    console.log("Value not found. Returning default value " + defaultValue);
    value = defaultValue;
    storageSet(name, value);
  }
  console.log("storageGet variable: " + name + " default value: " + defaultValue + " value: " + value);
  return value;
}

function gup(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if (results == null)
    return null;
  else
    return decodeURIComponent(results[1].replace(/\+/g, ' '));
}


function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function startsWith(string, query) {
  var regex = RegExp('^' + query, 'i');
  return regex.test(string);
}


function getHttpErrorText(s) {
  var regex = new RegExp("<h1>([^]+)<\/h1>");
  var match = regex.exec(s);
  if (match) {
    return match[1];
  }
  return s;
}

function isIpAddress(s) {
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(s)) {
    return (true)
  }
  return false;
}

function roundToTwo(num) {
  return +(Math.round(num + "e+2") + "e-2");
}
